//
//  LMLoading.m
//  LolaMarket
//
//  Created by Christian Olcina on 19/10/15.
//  Copyright (c) 2015 Javier Pino Rodríguez. All rights reserved.
//

#import "LMLoading.h"

@implementation LMLoading

const float fullAlpha = 1.0f;
const float midAlpha = 0.8f;

+(UIView *)createLoading:(UIView *)parent {
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"LMLoading" owner:self options:nil];
    UIView *loadingView = [nibObjects objectAtIndex:0];
    loadingView.tag = parent.hash;
    loadingView.frame = parent.bounds;
    //loadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    return loadingView;
}

+(void)addLoading:(UIView *)parent {
    [parent addSubview:[LMLoading createLoading:parent]];
}

+(void)showLoading:(UIView *)parent {
    [LMLoading showLoading:parent content:nil];
}
+(void)showLoading:(UIView *)parent content:(UIView *)content {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
      UIView *loadingView = [parent viewWithTag:parent.hash];
      //Add if not added
      if (!loadingView) {
        [LMLoading addLoading:parent];
      }
    
      loadingView.hidden = false;
      [parent bringSubviewToFront:loadingView];
      if (content) {
        content.alpha = midAlpha;
      }
    }];
}

+(void)hideLoading:(UIView *)parent {
    [LMLoading hideLoading:parent content:nil];
}
+(void)hideLoading:(UIView *)parent content:(UIView *)content {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        UIView *loadingView = [parent viewWithTag:parent.hash];
      //Add if not added
      if (!loadingView) {
        //[LMLoading addLoading:parent];
      } else {
        [parent sendSubviewToBack:loadingView];
        loadingView.hidden = true;
        if (content) {
            content.alpha = fullAlpha;
        }
      }
    }];
}

//Utility to get the correct parent to show loading
+(UIView *)getParentView:(UIViewController *)controller {
    UIView *parent = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        parent = controller.view;
    } else {
        parent = controller.navigationController.view;
    }
    return parent;
}

@end
