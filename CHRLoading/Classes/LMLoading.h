//
//  LMLoading.h
//  LolaMarket
//
//  Created by Christian Olcina on 19/10/15.
//  Copyright (c) 2015 Javier Pino Rodríguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LMLoading : NSObject

+(UIView *)createLoading:(UIView *)parent;
+(void)addLoading:(UIView *)parent;

+(void)showLoading:(UIView *)parent;
+(void)showLoading:(UIView *)parent content:(UIView *)content;

+(void)hideLoading:(UIView *)parent;
+(void)hideLoading:(UIView *)parent content:(UIView *)content;

+(UIView *)getParentView:(UIViewController *)controller;

@end
